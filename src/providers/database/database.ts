import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DatabaseProvider {

  constructor(private sqlite: SQLite) {  }

  public getDB() {
    return this.sqlite.create({
      name: 'infocli.db',
      location: 'default'
    });
  }

  public createDatabase() {
    return this.getDB()
      .then((db: SQLiteObject) => {
        this.createTables(db);
        this.insertDefaultItems(db);
      })
      .catch(e => console.log(e));
  }

  private createTables(db: SQLiteObject) {
    // Creating as tables
    db.sqlBatch([
      ['CREATE TABLE IF NOT EXISTS atletas (id integer primary key AUTOINCREMENT NOT NULL,'
                                           +' nome TEXT,'
                                            +'sobrenome TEXT,'
                                            +' nickname TEXT,'
                                             +'posicao TEXT)'],])
      .then(() => console.log('Tabelas criadas'))
      .catch(e => console.error('Erro ao criar as tabelas', e));
  }

  private insertDefaultItems(db: SQLiteObject) {
    
    db.executeSql('select COUNT(id) as qtd from eventos', [])
    .then((data: any) => {
      //If there is no record
      if (data.rows.item(0).qtd == 0) {

        // insert data
        db.sqlBatch([
          ['insert into atletas (nome, sobrenome, ninckname, posicao) values (?,?,?,?)', 
          [ 'João', 'de Tal', 'brTT', 'Top']]
        ])
        .then(() => console.log('event data included'))
        .catch(e => console.error('Error adding data', e));

      }
    })
    .catch(e => console.error('Error verifying qtd of events', e));
  }
}