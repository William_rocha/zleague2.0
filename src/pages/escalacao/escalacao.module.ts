import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EscalacaoPage } from './escalacao';

@NgModule({
  declarations: [
    EscalacaoPage,
  ],
  imports: [
    IonicPageModule.forChild(EscalacaoPage),
  ],
})
export class EscalacaoPageModule {}
