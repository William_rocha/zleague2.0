import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Modal, ModalOptions, LoadingController, AlertController} from 'ionic-angular';
import { ModalPage } from '../modal/modal';


@IonicPage()
@Component({
  selector: 'page-escalacao',
  templateUrl: 'escalacao.html',
})
export class EscalacaoPage {

  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modal: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {
  }

  ionViewDidLoad() {

  }

  openModal(posicao: string) {
    const myModalOptions: ModalOptions = {
      enableBackdropDismiss: false
    };

    const myModalData = {//Envia os parametros para a página Modal
      apelido: "brTT",
      posicao: posicao
    };

    const myModal: Modal = this.modal.create(ModalPage, { data: myModalData }, myModalOptions);

    myModal.present();

    myModal.onDidDismiss((data) => {
      console.log("I have dismissed");
      console.log(data);
    });

    myModal.onWillDismiss((data) => {
      console.log("I'm about to dismiss");
      console.log(data);
    });
  }
}
