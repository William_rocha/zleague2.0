import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
  apelido: string
  posicao: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }
  
  ionViewWillLoad() {
    const data = this.navParams.get('data');
    console.log(data);
    this.apelido = data.apelido;
    this.posicao = data.posicao
  }

  ionViewDidLoad() {
    const data = this.navParams.get('data');
    console.log('ionViewDidLoad ModalPage');
  }

  fechar() {
    this.viewCtrl.dismiss();
  }
}
