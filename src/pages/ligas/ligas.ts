import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LigasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ligas',
  templateUrl: 'ligas.html',
})
export class LigasPage {
  showCompLiga = true;
  showCompMercado = false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LigasPage');
  }

  public mercado() {
    this.showCompLiga = false;
    this.showCompMercado = true;        
  }
  
  public liga() {
    this.showCompLiga = true;
    this.showCompMercado = false;    
  }
}
