import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { EscalacaoPage } from '../pages/escalacao/escalacao';
import { LigasPage } from '../pages/ligas/ligas';
import { ModalPage } from '../pages/modal/modal';
import { PerfilPage } from '../pages/perfil/perfil';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { AngularFireAuthModule } from 'angularfire2/auth';
import { DatabaseProvider } from '../providers/database/database';
import { SQLite } from '@ionic-native/sqlite';
import { Camera } from '@ionic-native/camera';

export const firebaseConfig = {
  apiKey: "AIzaSyC3QZNJp428TqkGIYyprCLIKzPEmm38opw",
  authDomain: "fir-crud-33eb5.firebaseapp.com",
  databaseURL: "https://fir-crud-33eb5.firebaseio.com",
  storageBucket: "fir-crud-33eb5.appspot.com",
  messagingSenderId: "424282633948"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    EscalacaoPage,
    LigasPage,
    ModalPage,
    PerfilPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    
    //Import the AngularFireDatabaseModule to use database interactions
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    EscalacaoPage,
    LigasPage,
    ModalPage,
    PerfilPage
  ],
  providers: [
    SQLite,
    Camera,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider
  ]
})
export class AppModule {}
