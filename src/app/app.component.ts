import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { EscalacaoPage } from '../pages/escalacao/escalacao';
import { LoginPage } from '../pages/login/login';
import { LigasPage } from '../pages/ligas/ligas';
import { PerfilPage } from '../pages/perfil/perfil';

import { DatabaseProvider } from '../providers/database/database';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform,
             public statusBar: StatusBar,
             public splashScreen: SplashScreen,
             dbProvider: DatabaseProvider) {
    this.initializeApp();
    platform.ready().then(() => {
      statusBar.styleDefault();
      dbProvider.createDatabase()
        .then(() => {
          this.openHomePage(splashScreen);
        })
        .catch(() => {
          this.openHomePage(splashScreen);
        });
    });

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Dashboard', component: LoginPage },
      { title: 'Escalação', component: EscalacaoPage },
      { title: 'Ligas', component: LigasPage},
      { title: 'Perfil', component: PerfilPage}
    ];


  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  private openHomePage(splashScreen: SplashScreen) {
    splashScreen.hide();
    this.rootPage = HomePage;
  }
}
